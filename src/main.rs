// ping - simple Rust test of random, strings, sleep and hashes
// Copyright (C) 2018, Ronaldo Nascimento
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate rand;
extern crate nanoid;
extern crate md5;

use std::{thread, time};
// use std::io::{self, Write};
use rand::{thread_rng, Rng, ThreadRng};

mod listen;

const MAX_PACK: u32 = 1073741824;

fn random_pause(rng: &mut ThreadRng) -> u64 {
	let n: u64 = rng.gen_range(100, 500);
	return n;
}

fn get_host(rng: &mut ThreadRng) -> String {
	let mut host = String::new();
	let m1 = vec!["std", "ocx", "var", "sok", "tst", "opt"];
	host.push_str(rng.choose(&m1).unwrap());
	let m2 = rng.gen_range(1024, 9216);
	host.push_str(&format!("{:04}", m2));
	let m3 = vec!["psox", "pssf", "tvsf"];
	host.push_str(rng.choose(&m3).unwrap());
	host
}

fn get_char(rng: &mut ThreadRng) -> char {
	let v = vec!['-', '.', '.', '.', '.', '.', '*', '*', '+', '~'];
	*rng.choose(&v).unwrap()
}

fn main() {
    let mut rng = thread_rng();
    let mut ic: u32 = 0;
    let port: u32 = rng.gen_range(128, 8192);
    let host = get_host(&mut rng);

    println!("connecting to: {}:{}", host, port);

    let handler = thread::spawn(|| {
    	// thread code
    	listen::receive_thread();
	});

    loop {

    	let row_max: u32 = rng.gen_range(16, 32);

	    for _row in 1..row_max {
	    	ic += 1;
	    	print!("0x{:04x}: ", ic * 32);
	    	let mut message = String::new();
	    	for _col in 1..64 {
	    		message.push(get_char(&mut rng));
	    	}
	    	let digest = md5::compute(&message);
	    	println!("{}  --  {:x}", message, digest);
		    let pause = time::Duration::from_millis(random_pause(&mut rng));
		    thread::sleep(pause);
	    }

	    let id = nanoid::simple();
	    println!("sending packet: {} to {}", id, host);

	    if ic > MAX_PACK {
	    	break;
	    }
	}

	let res = handler.join();
	println!("{:?}", res);
}
