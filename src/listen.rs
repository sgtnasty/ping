// This file is part of ping.
//
// ping is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ping is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ping.  If not, see <https://www.gnu.org/licenses/>.

extern crate rand;

use std::{thread, time};
use rand::{thread_rng, Rng, ThreadRng};

fn long_pause(rng: &mut ThreadRng) -> u64 {
	let n: u64 = rng.gen_range(2000, 10000);
	return n;
}

pub fn receive_thread() {
	let mut rng = thread_rng();
	loop {
	    let pause = time::Duration::from_millis(long_pause(&mut rng));
	    thread::sleep(pause);
	    for _ in 1..rng.gen_range(4, 12) {
		    	let y = rand::random::<f64>();
		    	let z = rand::random::<f64>() * 2048.0;
	    	    println!("{}: {:.2}L", y, z);
	    }
	}
}